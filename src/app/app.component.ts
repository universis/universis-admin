import { Component } from '@angular/core';
import { ReferrerRouteService } from '@universis/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Universis Admin Panel';
  constructor(private referrer: ReferrerRouteService) {
  }
}
